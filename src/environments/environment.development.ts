function configValue<T>(configuredValue: string, defaultValue: T) {
	if (!configuredValue || configuredValue === ''){
		return defaultValue;
	} else {
		return configuredValue;
	}
}

let configuredEnvironment;
if (typeof window !== 'undefined') { // browser
	// @ts-expect-error This code should not error due to line 10
	configuredEnvironment = window?.env; // could be used for browser environment vars
} else {
	configuredEnvironment = {
		apiUrl: process.env['apiUrl']
	}
}

export const environment = {
	production: false,
	apiUrl: configValue<string>(configuredEnvironment?.apiUrl, 'http://localhost:4567')
};

console.debug('Using environment config')
console.debug(environment);