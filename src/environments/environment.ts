function configValue<T>(configuredValue: string | undefined, defaultValue: T) {
	if (!configuredValue || configuredValue === ''){
		return defaultValue;
	} else {
		return configuredValue;
	}
}

let configuredEnvironment;
if (typeof window !== 'undefined') { // browser	
	// window["env"] setup through file
	// @ts-expect-error This code should not error due to line 10
	if (window["env"]) {
		configuredEnvironment = {
			// @ts-expect-error This code should not error due to line 10
			apiUrl: window["env"]["apiUrl"] //window?.env // could be used for browser environment vars
		}
	} else {
		configuredEnvironment = {
			apiUrl: "http://localhost:4567"
		}
	}
} else {
	configuredEnvironment = {
		apiUrl: process.env['apiUrl']
	}
}

export const environment = {
	production: true,
	apiUrl: configValue<string>(configuredEnvironment?.apiUrl, 'http://backend:4567')
};
