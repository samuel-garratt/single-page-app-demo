import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router, RouterLink, RouterOutlet } from '@angular/router';
import { HousingService } from '../housing.service';
import { HousingLocation } from '../housing-location';
import { Title } from '@angular/platform-browser';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';

@Component({
  selector: 'app-details',
  standalone: true,
  providers: [HousingService, MessageService],
  imports: [
    CommonModule,
    RouterLink, 
    RouterOutlet,
    ButtonModule,
    ToastModule, 
    RippleModule],
  template: `
    <article>
    <p-toast />
    <img class="listing-photo" [src]="housingLocation?.photo"
      alt="Exterior photo of {{housingLocation?.name}}"/>
    <section class="listing-description">
      <h2 class="listing-heading">{{housingLocation?.name}}</h2>
      <p class="listing-location">{{housingLocation?.city}}, {{housingLocation?.state}}</p>
    </section>
    <section class="listing-features">
      <h2 class="section-heading">About this housing location</h2>
      <ul>
        <li>Units available: {{housingLocation?.availableUnits}}</li>
        <li>Does this location have wifi: {{housingLocation?.wifi}}</li>
        <li>Does this location have laundry: {{housingLocation?.laundry}}</li>
      </ul>
    </section>
    <section class="navigation">
      <div class="footer">      
        <a [routerLink]="['/']">Back</a>
        <p-button label="Delete" [loading]="isDeleting" 
          (onClick)="deleteHouse()" />
      </div>  
    </section>
  </article>
  `,
  styleUrl: './details.component.css'
})
export class DetailsComponent {
  route: ActivatedRoute = inject(ActivatedRoute);
  housingService = inject(HousingService);
  housingLocation: HousingLocation | undefined;
  housingLocationId: string;
  isDeleting = false;

  constructor(
    private title: Title, 
    private messageService: MessageService,
    private router: Router
  ) {
    this.housingLocationId = this.route.snapshot.params['id'];
    this.housingService.getHousingLocationById(this.housingLocationId).then(housingLocation => {
      this.housingLocation = housingLocation;
      this.title.setTitle(`House details - ${housingLocation?.name}`)
    });
  }

  async deleteHouse() {
    this.isDeleting = true;
    await this.housingService.deleteHousingLocation(this.housingLocationId);
    this.isDeleting = false;
    this.messageService.add({ 
      severity: 'success', 
      summary: 'Success', 
      detail: `Deleted housing location`
    });
    await new Promise((resolve) => setTimeout(resolve, 1200));
    this.router.navigate(["/"]);
  }
}
