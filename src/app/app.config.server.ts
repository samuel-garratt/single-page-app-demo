import { mergeApplicationConfig, ApplicationConfig } from '@angular/core';
import { provideServerRendering } from '@angular/platform-server';
import { appConfig } from './app.config';
import { provideHttpClient, withFetch, withInterceptors } from '@angular/common/http';
import { provideClientHydration } from '@angular/platform-browser';
import { loggingInterceptor } from './common/logging.interceptor';

const serverConfig: ApplicationConfig = {
  providers: [
    provideServerRendering(), 
    provideHttpClient(
      withFetch(),
      withInterceptors([
        loggingInterceptor
      ])
    ),
    provideClientHydration(),
  ]
};

export const config = mergeApplicationConfig(appConfig, serverConfig);
