import { ErrorHandler, Injectable } from "@angular/core";
import { LoggingService } from "./common/logging.service";

/**
 * TODO: This does not work for client side errors. Get to work rather than
 * custom calling
 */
@Injectable()
export class GlobalErrorHandler extends ErrorHandler {
  constructor(private loggerService: LoggingService) {
    super();
  }

  override handleError(error: unknown) {
    this.loggerService.log(error);
  }
}