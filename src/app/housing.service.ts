import { Injectable } from '@angular/core';
import { CreateHousingLocation, CreatedHouse, HousingLocation, LocationsList } from './housing-location';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { lastValueFrom, take } from 'rxjs';
import { LoggingService } from './common/logging.service';

@Injectable({
  providedIn: 'root'
})
export class HousingService {

  constructor(private http: HttpClient, private logger: LoggingService) { }

  readonly url = `${environment.apiUrl}/locations`;

  async getAllHousingLocations(): Promise<HousingLocation[] | undefined> {
    try {
      const request$ = this.http.get<LocationsList>(this.url).pipe(take(1));
      return (await lastValueFrom<LocationsList>(request$))._embedded.locations;
    } catch (error) {
      this.logger.log(error);  
      return undefined;
    }
  }
  
  async getHousingLocationById(id: string): Promise<HousingLocation | undefined> {
    try {
      const request$ = this.http.get<HousingLocation>(`${this.url}/${id}`, {
      }).pipe(take(1));
      return await lastValueFrom<HousingLocation>(request$) ?? [];
    } catch (error) {
      this.logger.log(error);  
      return undefined;
    }
  }

  async createHousingLocation(location: CreateHousingLocation) : Promise<CreatedHouse> {
    console.log(`Posting Location ${JSON.stringify(location)}`);
    const request$ = this.http.post<CreatedHouse>(this.url, location, {
      headers: {'Access-Control-Allow-Origin': '*' }
    }).pipe(take(1));
    return await lastValueFrom<CreatedHouse>(request$) ?? [];
  }

  async deleteHousingLocation(id: string) {
    const request$ = this.http.delete(`${this.url}/${id}`).pipe(take(1));
    return await lastValueFrom(request$) ?? [];
  }
}
