interface LogInfo {
	logglyKey?: string;
	sendConsoleErrors?: boolean;
	tag?: string;
	message?: string;
	browser?: string;
	stack?: string;
}

export class LogglyTracker {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  push(logInfo: LogInfo) : void {

  }
}