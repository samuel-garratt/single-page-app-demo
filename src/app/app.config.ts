import { ApplicationConfig, ErrorHandler, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';

import routeConfig from './routes';
import { provideClientHydration } from '@angular/platform-browser';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { BrowserAnimationsModule, provideAnimations } from '@angular/platform-browser/animations';
import { loggingInterceptor } from './common/logging.interceptor';
import { LoggingService } from './common/logging.service';
import { GlobalErrorHandler } from './global_error_handler';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routeConfig),
    provideHttpClient(
      withInterceptors(
        [
          loggingInterceptor
        ]
      )),    
    provideAnimations(),
    provideClientHydration(),
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    },
    // register teh service
    LoggingService,
    importProvidersFrom([BrowserAnimationsModule])
  ]
};