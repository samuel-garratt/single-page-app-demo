export interface Paging {
  current_page: number,
  page_size: number,
  total_count: number,
  total_pages: number
}

export interface LocationsList {
  paging: Paging;
  _embedded: {
    locations: HousingLocation[];
  };
}

export interface HousingLocation extends CreateHousingLocation {
  id: string | number;
}

export interface CreateHousingLocation {
  name: string;
  city: string;
  state?: string;
  photo: string;
  availableUnits: string | number;
  wifi: string | boolean;
  laundry: string | boolean;
}

export interface CreatedHouse {
  id: string
}
