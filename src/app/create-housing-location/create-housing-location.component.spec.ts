import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHousingLocationComponent } from './create-housing-location.component';
import { HousingService } from '../housing.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoggingService } from '../common/logging.service';

describe('CreateHousingLocationComponent', () => {
  let component: CreateHousingLocationComponent;
  let fixture: ComponentFixture<CreateHousingLocationComponent>;
  let housingService: HousingService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CreateHousingLocationComponent,
        HttpClientTestingModule
      ],
      providers: [
        LoggingService
      ]
    })
    .compileComponents();
    
    housingService = TestBed.inject(HousingService);
    spyOn(housingService, 'createHousingLocation').and.returnValue(Promise.resolve({
      id: "1"
		}))
    fixture = TestBed.createComponent(CreateHousingLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
