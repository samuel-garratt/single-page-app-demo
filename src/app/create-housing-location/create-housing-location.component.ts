import { Component, inject } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { HousingService } from '../housing.service';
import { Router } from '@angular/router';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';

interface HousingLocationForm {
  name: FormControl<string | null>;
  city: FormControl<string| null>;
  state: FormControl<string| null>;
  photo: FormControl<string| null>;
  availableUnits: FormControl<string| null>;
  wifi: FormControl<boolean| null>;
  laundry: FormControl<boolean| null>;
}

@Component({
  selector: 'app-create-housing-location',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule, 
    ToastModule, 
    RippleModule,
    ButtonModule],
  providers: [MessageService],
  templateUrl: './create-housing-location.component.html',
  styleUrl: './create-housing-location.component.css'
})
export class CreateHousingLocationComponent {
  housingService = inject(HousingService);

  createLocationForm = new FormGroup<HousingLocationForm>({
    name: new FormControl(null, [Validators.required]),
    city: new FormControl<string>(''),
    photo: new FormControl<string>(''),
    state: new FormControl<string>(''),
    availableUnits: new FormControl<string>(''),
    wifi: new FormControl<boolean>(false),
    laundry: new FormControl<boolean>(false),
  });

  constructor(private title: Title, 
    private router: Router,
    private messageService: MessageService
  ) 
  {
    this.title.setTitle(`Creating Housing Location ${title}`);
  }

  async createLocation() {
    this.createLocationForm.markAllAsTouched();
    if (!this.createLocationForm.valid) return;
    const values = this.createLocationForm.value;
    const mappedValues = {
      name: values.name ?? '',
      city: values.city ?? '',
      photo: values.photo ?? '',
      state: values.state ?? '',
      availableUnits: values.availableUnits ?? '',
      wifi: values.wifi ?? '',
      laundry: values.laundry ?? ''
    };
    console.log(mappedValues);
    const house = await this.housingService.createHousingLocation(mappedValues);
    if (house) {
      this.messageService.add({ 
        severity: 'success', 
        summary: 'Success', 
        detail: `Created location ${values.name}`
      });
      await new Promise((resolve) => setTimeout(resolve, 1200));
   
      this.router.navigate([`/details/${house.id}`]);
    }
  }

}
