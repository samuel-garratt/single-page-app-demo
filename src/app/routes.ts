import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DetailsComponent } from './details/details.component';
import { CreateHousingLocationComponent } from './create-housing-location/create-housing-location.component';

const routeConfig: Routes = [
	{
		path: '',
		component: HomeComponent,
		title: 'Home page'
	},
	{
		path: 'details/:id',
		component: DetailsComponent,
		title: 'Home details'
	},
	{
		path: 'create',
		component: CreateHousingLocationComponent,
		title: 'Create housing location'
	}
]

export default routeConfig;