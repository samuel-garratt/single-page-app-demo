import { HttpEvent, HttpInterceptorFn, HttpResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';

/**
 * This only seems to work for server side, not client side
 * @param req 
 * @param next 
 * @returns 
 */
export const loggingInterceptor: HttpInterceptorFn = (req, next) => {
  return next(req).pipe(
    tap((event: HttpEvent<unknown>) => {
      if (event instanceof HttpResponse) {
        console.log(`${req.method} ${event.url} ${event.statusText}`);
        console.log(event.headers);
      }
    })
  );
};
