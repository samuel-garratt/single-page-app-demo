import { Injectable } from "@angular/core";
import { LogglyTracker } from "../loggly-tracker";

/**
 * View logs at https://sentifydemo.loggly.com
 */
@Injectable()
export class LoggingService {
  // https://github.com/loggly/loggly-jslogger
  private loggly: LogglyTracker;

  constructor() {
    // @ts-expect-error This code should not error due to the first condition
    if (typeof window !== 'undefined' && typeof window?.LogglyTracker !== 'undefined') {
      this.loggly = new LogglyTracker();
      this.loggly.push({
        logglyKey: '7f6b27f4-40ec-4ce8-af29-3c73da81994d',
        sendConsoleErrors: true,
        tag: 'AngularJS-logs'
      });
    } else {
      this.loggly = new LogglyTracker();
    }
  }

  msg(message: string) {
    console.log(`Try to log msg ${message}`);
    // @ts-expect-error This code should not error due to the first condition
    if (typeof window !== 'undefined' && typeof window?.LogglyTracker !== 'undefined') {
      this.loggly.push({ message: message });
    }
  }

  log(error: unknown) {
    // @ts-expect-error This code should not error due to the first condition
    if (typeof window !== 'undefined'  && typeof window?.LogglyTracker !== 'undefined') {
      if (!this.loggly) this.loggly = new LogglyTracker();
      // @ts-expect-error Error could be given an interface perhaps
      this.loggly.push({ message: error?.message, browser: this.browserName(), stack: error?.stack });
    }
  }

  browserName() {
    if (typeof window === 'undefined') {
      return 'node';
    }
    return (function(agent){
      switch (true) {
          case agent.indexOf("edge") > -1: return "Edge";
          case agent.indexOf("edg") > -1: return "Edge (Chromium)";
          case agent.indexOf("chrome") > -1: return "Chrome";
          case agent.indexOf("trident") > -1: return "Internet Explorer";
          case agent.indexOf("firefox") > -1: return "Firefox";
          case agent.indexOf("safari") > -1: return "Safari";
          default: return "Other";
      }
    })(window.navigator.userAgent.toLowerCase());
  }
}