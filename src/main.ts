import { bootstrapApplication, provideClientHydration } from '@angular/platform-browser';
import { AppComponent } from './app/app.component';
import { provideRouter } from '@angular/router';
import routeConfig from './app/routes';
import { provideHttpClient } from '@angular/common/http';
import { ErrorHandler, importProvidersFrom } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoggingService } from './app/common/logging.service';
import { GlobalErrorHandler } from './app/global_error_handler';

bootstrapApplication(AppComponent,
    {providers: [
      provideRouter(routeConfig),
      provideHttpClient(), 
      provideClientHydration(),
      {
        provide: ErrorHandler,
        useClass: GlobalErrorHandler
      },     
      LoggingService,
      importProvidersFrom([BrowserAnimationsModule])
    ]})
  .catch(err => console.error(err));
