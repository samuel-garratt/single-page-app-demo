// If this is put into assets/config/env.js it will control
// which url the web uses to talk to the API
(function(window) {
	window["env"] = window["env"] || {};
	window["env"]["apiUrl"] = "http://localhost:4567";
})(this);