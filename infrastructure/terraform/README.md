* Initialise project

`terraform init`

* Validate project

`terraform validate`

* Create plan showing what will change

`terraform plan`

* Create plan as an artifact changing default variable

`terraform plan -var mock_backend=true -out=tfplan`

* Apply changes directly

`terraform apply`

* Apply changes from plan file

`terraform apply "tfplan"`

* Destroy changes, clean things up

`terraform destroy`