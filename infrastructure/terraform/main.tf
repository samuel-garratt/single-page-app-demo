# Terraform with mock backend. Update bottom image reference to use
# real backend if desired

locals {
  real_backend_image = "registry.gitlab.com/demo-pocs/schemaless_rest_api"
  mock_backend_image = "registry.gitlab.com/demo-pocs/single-page-app-demo:mock_backend"
}

resource "docker_container" "web" {
  name  = "spa-web-demo"
  image = docker_image.web.image_id
  ports {
    internal = 4000
    external = 4200
  }
  env = ["apiUrl=http://backend:4567"]
  networks_advanced {
	  name = docker_network.spa_demo.name
  }
}

resource "docker_container" "backend" {
  name  = "backend"
  image = docker_image.backend.image_id
  ports {
    internal = 4567
    external = 4567
  }
  networks_advanced {
	  name = docker_network.spa_demo.name
  }
  env = var.mock_backend ? [] : ["models=['locations']"]
}

resource "docker_network" "spa_demo" {
  name = "spa-demo"
}

resource "docker_image" "web" {
  name = "spa-web-demo:latest"
  build {
    context = "../.."
    tag     = ["web:latest"]
  }
}

resource "docker_image" "backend" {
  name = var.mock_backend ? local.mock_backend_image : local.real_backend_image
}
