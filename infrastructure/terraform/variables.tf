variable "mock_backend" {
  type        = bool
  description = "Whether a mock backend is used"
  default = false
}
