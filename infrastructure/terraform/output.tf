output "web" {
  value = "http://localhost:${docker_container.web.ports[0].external}"
}

output "backend" {
  value = "http://localhost:${docker_container.backend.ports[0].external}/locations"
}