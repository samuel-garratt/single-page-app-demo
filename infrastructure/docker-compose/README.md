* Validate and view docker-compose file

```
docker-compose config
```

* Build web docker image

```
docker-compose build
```

* Spin up UI with real backend

```
docker-compose up
```

The process can be stopped by `Crtl+c`. 

* Stop and clean up docker-compose service

`docker-compose down` will stop all services and clean up the network.

* Spin up UI with mock backend

```
docker-compose -f mock.compose.yaml up
```

* Inspect docker-compose from another thread

If you run docker-compose in 1 terminal, in another run

```
docker-compose ps
```

to see the services running, their state, etc

* Security tests

Run this to spin up the application, run DAST security tests on it and then create a report.

```
docker-compose -f compose-security-test.yaml up --abort-on-container-exit --exit-code-from test
```

See the report in `zap/testreport.html` of this repo.

# TODO:
* Docker-compose run playright e2e tests