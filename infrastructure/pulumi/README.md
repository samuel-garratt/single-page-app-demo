* Preview changes

`pulumi preview`

* Create infrastructure

`pulumi up`

* View stack details  

`pulumi stack ls`

* Destroy infrastructure

`pulumi down`

## Other docs

Compare terraform and pulumi

https://www.pulumi.com/docs/concepts/vs/terraform/terminology/