import * as pulumi from "@pulumi/pulumi";
import * as docker from "@pulumi/docker";

// Create a Docker network
const network = new docker.Network("spa-demo", {
    name: "spa-demo",
    driver: "bridge",
});

// Build a Docker image from a Dockerfile in this repo
const uiImage = new docker.Image("web", {
    build: {
        context: "../..",
        dockerfile: "../../Dockerfile",
        platform: "linux/amd64",
    },
    imageName: "registry.gitlab.com/samuel-garratt/web:latest",
    skipPush: true,
});

const externalWebPort = 4200;

// Create a container using the built image
const webContainer = new docker.Container("web", {
    image: uiImage.imageName,
    name: "spa-web-demo",
    networksAdvanced: [{
        name: network.name,
    }],
    ports: [{
        external: externalWebPort,
        internal: 4000
    }],
    envs: ["apiUrl=http://backend:4567"]
});

// Pull a Docker image from a remote repository
const backendImage = new docker.RemoteImage("backendImage", {
    name: "registry.gitlab.com/demo-pocs/single-page-app-demo:mock_backend",
});

const externalApiPort = 4567;

// Create a container using the pulled image
const backendContainer = new docker.Container("backendContainer", {
    image: backendImage.name,
    name: "backend",
    networksAdvanced: [{
        name: network.name,
    }],
    ports: [{
        external: externalApiPort,
        internal: 4567
    }]
});

// Export container names
export const webContainerName = webContainer.name;
export const backendContainerName = backendContainer.name;
export const webUrl = `http://localhost:${externalWebPort}`
export const apiUrl = `http://localhost:${externalApiPort}`
