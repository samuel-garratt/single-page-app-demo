Kubernetes (k8) is a generic technology that can be run on any platform: on a local machine, Azure, AWS, Google, etc. 

It provides powerful abstractions for managing applications, their configuration, networking, storage and everything required to serve and monitor them. 

The same configuration can be tested in a docker node such as `kind` and applied to `production`.

The [helm](helm) folder shows a package and templating tool for managing a group of kubernetes resources and defining values that can vary according to different environments and needs.

# Creating a cluster locally

kind or Minikube can be used to host a k8 cluster locally
These instructions use kind which can be installed at https://kind.sigs.k8s.io.

With `kind` a cluster can be created with
```
kind create cluster --config=./kind-config.yaml
```

> If you want to load docker images from your local machine into KIND to avoid re-pulling or to use a locally built version see the section `LOAD images into KIND` at the bottom of this file

# Installing kubectl
In order to interact with the cluster, it's recommended you install `kubectl` and an IDE like LENS or k9.

# Installing/Upgrading helm chart

From within [helm/spa-demo](helm/spa-demo) folder run

```
helm upgrade spa . --debug --wait --install
```

to run this local chart into your cluster.

Install ingress controller to view the application by running

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
```

Use the following command to wait until the nginx ingress controller is ready

```
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s
```	 

Now see the application running at `http://localhost`

# Uninstall helm chart 

```
helm uninstall spa
```

# Removing KIND cluster

Once finished, the cluster can be created with

```
kind delete cluster
```

## LOAD images into KIND from local machine

You can load images from your machine into kind with the commands

```
kind load docker-image registry.gitlab.com/demo-pocs/schemaless_rest_api:latest
```

You can retag a locally built image with `docker tag web:latest registry.gitlab.com/demo-pocs/single-page-app-demo:latest` and then load that into kind to save pulling it again

```
kind load docker-image registry.gitlab.com/demo-pocs/single-page-app-demo:latest
```

## Scaffold and minikube

Perhaps using Scaffold would be helpful demonstrating the FileSync https://skaffold.dev/docs/filesync/ ability to update kubernetes code automatically.

https://testingclouds.wordpress.com/2021/03/09/migrating-from-docker-compose-to-skaffold/