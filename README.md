A very simple demo Single Page Application that relies on a backend that can be toggled between a virtual and mock one, has some unit and e2e tests and various methods of hosting it locally. See [purpose](#purpose-of-this-repolab) for more details.

# Prequisities
* Install docker (allow volumes on the workspace you are serving from)
* To run UI without docker install node and angular (`npm install -g @angular/cli`)

# Quick start
To quickly run the application clone this repo, navigate to `infrastructure/docker-compose` and run 

* (Optional to avoid some warnings) Copy local config settings into your `config` file
  * `mkdir src/assets/config`
  * `cp src/assets/config-local.js src/assets/config/env.js`
* `docker-compose -f mock.compose.yaml up` to spin up the UI with mock backend 
* `docker-compose up` to spin up UI with real backend

* See the system up and running
Open `http://localhost:3000` to see the homepage dashboard
Open `http://localhost:4200` to see the UI
Open `http://localhost:4567/locations` to see the Backend
* Move files from `mock_backend/locations/strategies` up one folder to be directly under `mock_backend/locations` and move `list.json` out to have the mock backend updated. Refresh UI to see the change

# Other configurations

* Run real service that can store and return house location data

```
docker run -v $(pwd):/home/app/serve -p 4567:4567 --rm -it registry.gitlab.com/demo-pocs/schemaless_rest_api
```

You can create data by performing a POST to `http://localhost:4567/locations` with the JSON body

```json
{
	"id": 3,
	"name": "Acme Fresh Start Housing",
	"city": "Chicago",
	"state": "IL",
	"photo": "https://angular.io/assets/images/tutorials/faa/bernard-hermant-CLKGGwIBTaY-unsplash.jpg",
	"availableUnits": 4,
	"wifi": true,
	"laundry": true
}
```

See the list of locations by performing a GET to `http://localhost:4567/locations`.

* Run the single page application

  * Install dependencies

```
npm install
```
  * Host server
Set `apiUrl` environment variable to change where backend is. By default this is localhost:4567 so no need if the above docker command is used.

```
ng serve
```

* Run mock backend 

```
docker run -v $(pwd):/home/app/serve -p 4567:4567 --rm -it registry.gitlab.com/samuel-garratt/file_sv
```

This creates a mock backend based on the folder `mock_backend`. You can simulate different conditions by moving files from `mock_backend/locations/strategies` up one folder. The docker image will automatically reload it's mocks without needing a restart.

# Running tests 

## Run unit tests

```
ng test
```

## Run e2e tests
Ensure the mock backend is up at localhost:4567 and then run

```
npx playwright test tests/service
```

> This will serve the application at localhost:4200 while it is running.

These are service level tests as the backend is mocked. 

To run the end to end tests, run the real backend and use

```
npx playwright test tests/e2e
```

To see the browser pop up while running these tests run and debug them run

```
npx playwright test tests/service --project chromium --debug
```

or run in UI mode with 

```
npx playwright test --ui
```

# Monitoring

Client error logs are sent to Loggly at https://sentifydemo.loggly.com. 
Ask for access if you want to see such logs.

# Purpose of this repo/lab

The purpose of this repo is to be a form of a lab with a simple setup of a front end and backend that people can try out new ideas on. The smallness of it, ideally makes it easier to grasp what is going on. 

There are 2 backends which the infrastructure folder serving methods explain toggling between.

* The 'real' backend has no validation or business rule logic. It just performs basic CRUD on a 'locations' object, simply returning whatever data is entered into it using a [simple REST API](https://gitlab.com/demo-pocs/schemaless_rest_api). As such, this is not an example of a realistic API that would ever be used in production. 
* An optional mock backend that demonstrates how [file_sv](https://gitlab.com/samuel-garratt/file_sv) can be used to return different statuses, list sizes, and delays from the backend to see how the frontend behaves. See the 'mock_backend' folder for the folders and files that defines it. The 'location/strategies' has different files that can be used to simulate different responses to the UI.

Due the simplicity of the backend, the focus of this lab is more on playing with infrastructure, the frontend code and the testing of its simple functionality.

The `infrastructure` folder has various sub folders with different methods of hosting this frontend and backend with tools such as docker-compose, terraform, pulumi and helm (kubernetes). One can add different docker containers to these to add more traceability, monitoring, observability, etc. 

The Angular app itself in the [src](src) folder has unit tests which could be enhanced to practise increasing and improving test coverage run through 'npm run test'.

The 'tests' folder has protractor e2e tests one can use to practice such tests on a simple application. These tests are also run in a gitlab pipeline that also can be an example and playpen. Such tests demonstrate how testing can be done on the application source code itself (within a pull request), rather than against an already deployed application, which finds issues later and bugs then often are not coupled with the change that caused them. It is hoped that through such a demo more testing left, testing early could be achieved.

> One can also practice adding other test frameworks, infrastructure deployment methods to do this. This is the key purpose of this lab, a place with a simple > application, that one can understand, add to and play with without pressure of getting things wrong, taking too long, making mistakes that a client may not > allow time for.