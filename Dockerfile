FROM node:18-alpine as build
WORKDIR /app/src
COPY package*.json ./
RUN npm ci
COPY . ./
RUN npm run build
RUN mkdir -p src/assets/config
COPY src/assets/config-backend.js src/assets/config/env.js

FROM node:18-alpine
WORKDIR /usr/app
COPY --from=build /app/src/dist/browser /usr/browser/
COPY --from=build /app/src/dist/server ./
CMD node server.mjs
EXPOSE 4000