import { test, expect } from '@playwright/test';

/** Plan to use this in FileSV in the future */
test.use({
	extraHTTPHeaders: {
		'file_sv': 'success',
	}
});

test('create home handles data and navigates to get location', async ({ page }) => {
	await page.goto('/');
	await page.getByTestId('create-location').click();
	await page.getByLabel('Name').fill('New Location');
	await page.getByLabel('City').fill('Test City');
	await page.getByLabel('State').fill('Stately');
	await page.getByLabel('the Photo').fill('https://angular.io/assets/images/tutorials/faa/bernard-hermant-CLKGGwIBTaY-unsplash.jpg');
	await page.getByLabel('Units').fill('2');
	await page.getByLabel('Wifi').check();
	await page.locator('#laundry').check();
	await page.getByRole('button', { name: 'Create' }).click();
	await expect(page.getByRole('article')).toContainText('Acme Fresh Start Housing');
	await expect(page.getByRole('paragraph')).toContainText('Chicago, IL');
	await expect(page.getByRole('list')).toContainText('Units available: 4');
	await expect(page.getByRole('list')).toContainText('Does this location have wifi: true');
	await expect(page.getByRole('list')).toContainText('Does this location have laundry: true');
});
