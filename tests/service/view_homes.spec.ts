import { test, expect } from '@playwright/test';

test.describe('Homes app', () => {
  test('should load default route', async ({page}) => {
    await page.goto('/');

    await expect(page).toHaveTitle('Home page');
  });

  test('should open a home when selecting it', async ({page}) => {
    const id = 1;
    const expectedName = 'House details - Acme Fresh Start Housing'
    await page.goto('/');
    await page.getByTestId(`open-${id}`).click();
    await expect(page).toHaveTitle(expectedName);
  })
});
