import { test, expect } from '@playwright/test';

// Create, read, delete - all in 1 go. 
// Not really preferable as best for test to be atomic
test('create, read and then delete', async ({ page }) => {
  await page.goto('/');
  await page.getByTestId('create-location').click();
  await page.getByLabel('Name').fill('Test Location');
  await page.getByLabel('City').fill('Test City');
  await page.getByLabel('State').fill('Test State');
  await page.getByLabel('the Photo').fill('https://angular.io/assets/images/tutorials/faa/bernard-hermant-CLKGGwIBTaY-unsplash.jpg');
  await page.getByLabel('Units').fill('3');
  await page.getByLabel('Wifi').check();
  await page.locator('#laundry').check();
  // Create location, capturing API response
  const responsePromise = page.waitForResponse('**/locations');
  await page.getByRole('button', { name: 'Create' }).click();
  const response = await responsePromise;
  const createdLocation: { id: string } = JSON.parse(await response.text());

  // Verify location on created page
  await expect(page.getByRole('article')).toContainText('Test Location');
  await expect(page.getByRole('paragraph')).toContainText('Test City, Test State');
  await expect(page.getByRole('list')).toContainText('Units available: 3');
  await expect(page.getByRole('list')).toContainText('Does this location have wifi: true');
  await expect(page.getByRole('list')).toContainText('Does this location have laundry: true');
  
  // Back to dashboard
  await page.getByRole('link', { name: 'Back' }).click();
  // From dashboard open
  await page.getByTestId(`open-${createdLocation.id}`).click();
  // Delete
  await page.getByRole('button', { name: 'Delete' }).click();
  // Assert id no longer present
  await expect(page.getByText(`open-${createdLocation.id}`)).toHaveCount(0);
});